# nfcapdu
A simple tool to exchange APDUs with an RFID/NFC tag

This program allows you to create an interactive session to send APDUs to a tag and receive responses. I wrote this to easily experiment around ST25TA tags and test APDUs before developing more specific code. Maybe this can be useful for someone else.

- It supports the creation of aliases (defined in `~/.nfcapdurc`) to simplify the sending of common commands
- APDU history is kept between sessions (in `~/.nfcapdu_history`)
- Have completion for commands and aliases
- You can set default modulation and baud rate in config file (see `nfcapdurc_sample`)
- You can enable or disable colors in config file (see `nfcapdurc_sample`)
- You can switch mode (ADPU or RAW) with `rawmode` and `apdumode` (default)
- Support for Lua scripts with `-s FILE`. Take a look at `sample.lua` for more information
- Quit with `quit` or Ctrl+d (EOF)


Exemple:

```
$ ./nfcapdu
NFC reader: ASK / LoGO (pn53x_usb:002:033) opened
ISO/IEC 14443A (106 kbps) tag found. UID: 02C4004E447224
4 aliases loaded
APDU> alias
Defined aliases:
  selst25app = 00a4 0400 07 d2760000850101 00
  selstfile = 00a4 000c 02 e101
  selccfile = 00a4 000c 02 e103
  readstfile = 00b0 0000 12
APDU> selst25app
=> 00 a4 04 00 07 d2 76 00 00 85 01 01 00
<= 90 00
APDU> 00a4 000c 02 e101
=> 00 a4 00 0c 02 e1 01
<= 90 00
APDU> readstfile
=> 00 b0 00 00 12
<= 00 12 01 00 11 00 81 02 02 c4 00 4e 44 72 24 1f ff c4 90 00
APDU> 00b0 0000 aa
=> 00 b0 00 00 aa
<= 62 82
Error: End of file/record reached before reading Le bytes (0x6282)
cardtransmit error!
APDU>
```

Raw mode exemple, start with APDU mode (default) and alias :

```
$ ./nfcapdu
NFCapdu v0.0.3 - Copyright (c) 2022-2023 - Denis Bodor
This is free software with ABSOLUTELY NO WARRANTY.
NFC reader: ASK / LoGO (pn53x_usb:003:007) opened
ISO/IEC 14443A (106 kbps) tag found. UID: 02C4004E447224
Logging enabled (/home/denis/nfcapdu.log openned)
21 alias loaded
Use 'help' to get a list of available commands.
APDU> selapp
=> 00 a4 04 00 07 d2 76 00 00 85 01 01 00
<= 90 00
```

Switch to raw mode. APDU no longer works, we need to add `02` because the PN53x no longer does it for us (no easy framing):

```
APDU> rawmode
1 target(s) deselected
ISO/IEC 14443A (106 kbps) tag found. UID: 02C4004E447224
RAW> selapp
=> 00 a4 04 00 07 d2 76 00 00 85 01 01 00
nfc_initiator_transceive_bytes error! RF Transmission Error
cardtransmit error!

RAW> 0200 a4 04 00 07 d2 76 00 00 85 01 01 00
=> 02 00 a4 04 00 07 d2 76 00 00 85 01 01 00
<= 02 90 00
```

Switch back to APDU mode. `02` is no longer needed and causes an error, but the APDU corresponding to the alias now works:

```
RAW> apdumode
1 target(s) deselected
ISO/IEC 14443A (106 kbps) tag found. UID: 02C4004E447224
APDU> 0200 a4 04 00 07 d2 76 00 00 85 01 01 00
=> 02 00 a4 04 00 07 d2 76 00 00 85 01 01 00
<= 6e 00
Error: Class not supported (0x6e00)
cardtransmit error!
APDU> selapp
=> 00 a4 04 00 07 d2 76 00 00 85 01 01 00
<= 90 00
APDU>

```

Raw mode is useful for configuring an Ultimate Magic Card for example (see [Notes on Magic Cards](https://github.com/RfidResearchGroup/proxmark3/blob/master/doc/magic_cards_notes.md) from RfidResearchGroup Proxmark3 documentation). Example, to dump the configuration we use:

```
$ ./nfcapdu
NFCapdu v0.0.3 - Copyright (c) 2022-2023 - Denis Bodor
This is free software with ABSOLUTELY NO WARRANTY.
NFC reader: ASK / LoGO (pn53x_usb:003:007) opened
ISO/IEC 14443A (106 kbps) tag found. UID: 04223344
Logging enabled (/home/denis/nfcapdu.log openned)
21 alias loaded
Use 'help' to get a list of available commands.

APDU> rawmode
1 target(s) deselected
ISO/IEC 14443A (106 kbps) tag found. UID: 04223344

RAW> CF00000000C6
=> cf 00 00 00 00 c6
<= 01 00 00 00 00 00 02 09 09 78 00 91 02 bd ac 19 13 10 11 12 13 14 15 16 04 00 18 02 ff 00 03 fc

RAW> quit
Bye. Have a nice day
```

I recently added Lua scripting features (Take a look at `sample.lua`):

```
$ ./nfcapdu -s sample.lua
NFCapdu v0.1 - Copyright (c) 2022-2023 - Denis Bodor
This is free software with ABSOLUTELY NO WARRANTY.
NFC reader: NXP / PN533 (pn53x_usb:001:003) opened
ISO/IEC 14443A (106 kbps) tag found. UID: 02C4004E447224
Logging enabled (/home/denis/nfcapdu.log openned)

Tag UID: 02c4004e447224
> Select ST25TA app
9000
> Select ST25TA ST File
9000
```
