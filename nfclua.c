#include <stdlib.h>
#include <err.h>
#include <errno.h>
#include <string.h>
#include <nfc/nfc.h>
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

#include "nfcapdu.h"
#include "nfclua.h"

extern nfc_device *pnd;
extern size_t conf_rapdumaxsz;
extern lua_State *L;
extern uint8_t luataguid[];
extern size_t luataguidlen;

// Transmit ADPU from hex string
int
lua_sendstrapdu(lua_State *L)
{
	int i;
	uint8_t *resp;
	size_t respsz;

	const char *strapdu;
	size_t strapdulen = 0;

	luaL_checktype(L, 1, LUA_TSTRING);

	strapdu = lua_tostring(L, -1);
	strapdulen = lua_rawlen(L, -1);
	lua_pop(L, 1);

	if (!strapdulen)
		return(0);

	// allocate R-APDU buffer
	if ((resp = malloc(sizeof(uint8_t) * conf_rapdumaxsz)) == NULL) {
		warn("Lua resp[] malloc Error: ");
		failquit();
	}
	respsz = conf_rapdumaxsz;

	// Send APDU-C
	if (strcardtransmit(pnd, strapdu, resp, &respsz, 1) < 0) {
		warnx("Lua strcardtransmit error!");
		free(resp);
		return(0);
	}

	// return APDU-R
	lua_createtable(L, respsz, 0);
	for (i = 0; i < respsz; i++) {
		lua_pushinteger(L, resp[i]);
		lua_rawseti(L, -2, i + 1);
	}

	free(resp);

	return(1);
}

// Transmit ADPU from int array
int
lua_sendapdu(lua_State *L)
{
	uint8_t *capdu;
	size_t capdulen;
	uint8_t *resp;
	size_t respsz;
	lua_Integer data;
	int i, len, j = 0;

	luaL_checktype(L, 1, LUA_TTABLE);

	len = luaL_len(L, 1);
	if (!len) {
		warnx("Invalid APDU!");
		return(0);
	}

	// allocate C-APDU buffer
	if ((capdu = malloc(sizeof(uint8_t) * len)) == NULL) {
		warn("Lua capdu[] malloc Error: ");
		failquit();
	}
	capdulen = len;

	for (i = 1; i <= len; i++) {
		lua_rawgeti(L, 1, i);
		data = lua_tointeger(L, -1);
		lua_pop(L, 1);
		if (data < 0 || data > 0xff) {
			warnx("Bad hex value in sendapdu() Lua call! Abording...");
			return(0);
		}
		capdu[j] = (uint8_t)data;
		j++;
	}
	lua_pop(L, 1);

	// allocate R-APDU buffer
	if ((resp = malloc(sizeof(uint8_t) * conf_rapdumaxsz)) == NULL) {
		warn("Lua resp[] Malloc Error: ");
		failquit();
	}
	respsz = conf_rapdumaxsz;

	// Send APDU-C
	if (cardtransmit(pnd, capdu, capdulen, resp, &respsz, 1) < 0) {
		warnx("Lua cardtransmit error!");
		free(resp);
		free(capdu);
		return(0);
	}

	// return APDU-R
	lua_createtable(L, respsz, 0);
	for (i = 0; i < respsz; i++) {
		lua_pushinteger(L, resp[i]);
		lua_rawseti(L, -2, i + 1);
	}

	free(capdu);
	free(resp);

	return(1);
}

int
lua_getuid(lua_State *L)
{
	int i;

	lua_createtable(L, luataguidlen, 0);

	for (i = 0; i < luataguidlen; i++) {
		lua_pushinteger(L, luataguid[i]);
		lua_rawseti(L, -2, i + 1);
	}

	return(1);
}
