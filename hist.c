#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <err.h>
#include <errno.h>
#include <readline/history.h>
#include <nfc/nfc.h>

#include "nfcapdu.h"
#include "hist.h"

extern int conf_histsize;

void
apdu_inithistory(char **file)
{
	char *home;
	char *p;
	int histpathsz;

	if ((home = getenv("HOME")) == NULL) {
		warnx("Unable to get $HOME");
		exit(EXIT_FAILURE);
	}

	histpathsz = strlen(home) + 1 + strlen(HISTFILE) + 1;
	if ((p = (char *) malloc(histpathsz)) == NULL) {
		err(EXIT_FAILURE, "Memory allocation error: ");
	}

	if (snprintf(p, histpathsz, "%s/%s", home, HISTFILE) != histpathsz - 1) {
		warnx("History file path error");
	}

	if( read_history(p) != 0) {
		if(errno == ENOENT) {
			write_history(p);
		} else {
			err(EXIT_FAILURE, "load hist error: ");
			exit(EXIT_FAILURE);
		}
	}

	stifle_history(conf_histsize);

	*file = p;
}

void
apdu_closehistory(char *file)
{
	if (write_history(file) != 0) {
		warn("write history error: ");
	}
	free(file);
}

void
apdu_addhistory(char *line)
{
	HIST_ENTRY *entry = history_get(history_length);
	if ((!entry) || (strcmp(entry->line, line) != 0))
		add_history(line);
}

void
apdu_disphist()
{
	int i;
	HIST_ENTRY **hist = history_list();

	for (i = 0; i < history_length; i++) {
		printf("  %s\n", hist[i]->line);
	}
}


