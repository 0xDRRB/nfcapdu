#include <stdio.h>
#include <ctype.h>
#include <stdint.h>
#include <err.h>
#include <string.h>
#include <nfc/nfc.h>
#include "nfcapdu.h"
#include "util.h"

int
isblankline(char *line)
{
	char *ch;
	int is_blank = 1;
	for (ch = line; *ch != '\0'; ch++){
		if (!isspace(*ch)) {
			is_blank = 0;
			break;
		}
	}
	return is_blank;
}

void
printhelp(char *binname)
{
	printf(NFCAPDUVERSION);
	printf("Usage : %s [OPTIONS]\n", binname);
	printf(" -r              start in raw mode (Easy framing disabled)\n");
	printf(" -s <FILE>       load and run Lua script\n");
	printf(" -l              list available readers\n");
	printf(" -d connstring   use this device (default: use the first available device)\n");
	printf(" -h              show this help\n");
}

void
apdu_help()
{
	printf("Commands:\n");
	printf("        alias - shows the aliases defined in ~/.nfcapdurc\n");
	printf("      history - display command history\n");
	printf("      rawmode - switch to RAW mode (no Easy framing) and reselect target\n");
	printf("     apdumode - switch to APDU mode (Easy framing enable) and reselect target\n");
	printf(" quit (or ^D) - exit program\n");
	printf("         help - this\n");
	printf("anything else - treated as an APDU to send in hex (blanks are ignored)\n\n");
}

size_t
hex2array(const char *line, uint8_t *data, size_t len)
{
	size_t datalen = 0;
	uint32_t temp;
	int indx = 0;
	char buf[5] = { 0 };

	if (strlen(line) < len*2)
		return(0);

	while (line[indx]) {
		if (line[indx] == '\t' || line[indx] == ' ') {
			indx++;
			continue;
		}

		if (isxdigit(line[indx])) {
			buf[strlen(buf) + 1] = 0x00;
			buf[strlen(buf)] = line[indx];
		} else {
			// we have symbols other than spaces and hex
			return(0);
		}

		if (strlen(buf) >= 2) {
			sscanf(buf, "%x", &temp);
			data[datalen] = (uint8_t)(temp & 0xff);
			*buf = 0;
			datalen++;
			if (datalen > len)
				return(0);
		}

		indx++;
	}

	// no partial hex bytes
	if (strlen(buf) > 0 ) {
		warnx("Error: odd number of hex values!");
		return(0);
	}

	return(datalen);
}

