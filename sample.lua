-- Application Select for ST25TA tag
st25selapp = "00a4 0400 07 d2760000850101 00"
selstfile = { 0x00, 0xa4, 0x00, 0x0c, 0x02, 0xe1, 0x01 }

uid = getuid();
s = "Tag UID: "
for key,value in ipairs(uid) do
	s = s .. string.format("%02x", value)
end
print(s)



print("> Select ST25TA app")
-- Send string APDU
response = sendstrapdu(st25selapp)
-- Display response
s = ""
for key,value in ipairs(response) do
	s = s .. string.format("%02x", value)
end
print(s)

print("> Select ST25TA ST File")
-- Send hex APDU
response2 = sendapdu(selstfile)
-- Display response
s = ""
for key,value in ipairs(response2) do
	s = s .. string.format("%02x", value)
end
print(s)

