#include <time.h>
#include <math.h>
#include <stdio.h>
#include <stdint.h>
#include <err.h>
#include <errno.h>
#include <nfc/nfc.h>
#include "nfcapdu.h"
#include "log.h"

extern int conf_log;
extern FILE *fplog;

void
apdu_logapdu(uint8_t *apdu, size_t apdulen, int out)
{
	size_t szPos;

	if (!conf_log || !fplog)
		return;

	struct timespec spec;
	long ms;

	clock_gettime(CLOCK_REALTIME, &spec);

	time_t t = spec.tv_sec;
	struct tm tm = *localtime(&t);
	ms = floor(spec.tv_nsec / 1.0e6);

	if (fprintf(fplog, "[%02d:%02d:%02d.%03ld] ", tm.tm_hour, tm.tm_min, tm.tm_sec, ms) == -1)
		warnx("Log write error!");

	if (fprintf(fplog, "%c ", out ? '>' : '<') == -1)
		warnx("Log write error!");

	for (szPos = 0; szPos < apdulen; szPos++) {
		if (fprintf(fplog, "%02x ", apdu[szPos]) == -1)
			warnx("Log write error!");
	}

	if (fprintf(fplog, "\n") == -1)
		warnx("Log write error!");

	fflush(fplog);
}

void
apdu_logstart()
{
	if (!conf_log || !fplog)
		return;

	time_t t = time(NULL);
	struct tm tm = *localtime(&t);

	if (fprintf(fplog, "--- Begin session (%d-%02d-%02d %02d:%02d:%02d)\n",
				tm.tm_year+1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec) == -1)
		warnx("Log write error!");

	fflush(fplog);
}

void
apdu_logstop()
{
	if (!conf_log || !fplog)
		return;

	time_t t = time(NULL);
	struct tm tm = *localtime(&t);

	if (fprintf(fplog, "--- End session (%d-%02d-%02d %02d:%02d:%02d)\n",
				tm.tm_year+1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec) == -1)
		warnx("Log write error!");

	fflush(fplog);
}

void
apdu_logmode(int mode)
{
	if (!conf_log || !fplog)
		return;

	struct timespec spec;
	long ms;

	clock_gettime(CLOCK_REALTIME, &spec);

	time_t t = spec.tv_sec;
	struct tm tm = *localtime(&t);
	ms = floor(spec.tv_nsec / 1.0e6);

	if (fprintf(fplog, "[%02d:%02d:%02d.%03ld] ", tm.tm_hour, tm.tm_min, tm.tm_sec, ms) == -1)
		warnx("Log write error!");

	if (fprintf(fplog, "switched to %s mode\n", mode == APDUMODE ? "APDU" : "RAW") == -1)
		warnx("Log write error!");

	fflush(fplog);
}
