#pragma once

#define NFCAPDUVERSION "NFCapdu v0.1 - Copyright (c) 2022-2023 - Denis Bodor\nThis is free software with ABSOLUTELY NO WARRANTY.\n"

#define CONFFILE    ".nfcapdurc"
#define HISTFILE    ".nfcapdu_history"
#define LOGFILE     "./nfcapdu.log"

#define HISTSIZE             128
#define RAPDUMAXSZ           512
#define CAPDUMAXSZ           512
#define COLOR                  1
#define LOG                    0
#define MODTYPE    NMT_ISO14443A
#define NBR              NBR_106
#define S_SUCCESS         0x9000  // Command completed successfully
#define S_OK              0x9100  // OK (after additional data frame)
#define S_MORE            0x91af  // Additional data frame is expected to be sent

#define APDUMODE			   0
#define RAWMODE				   1

char **rl_commands_completion(const char *text, int start, int end);
char *rl_commands_generator(const char *text, int state);
int apfu_initconfig();
int strcardtransmit(nfc_device *pnd, const char *line, uint8_t *rapdu, size_t *rapdulen, int quiet);
int cardtransmit(nfc_device *pnd, uint8_t *capdu, size_t capdulen, uint8_t *rapdu, size_t *rapdulen, int quiet);
void failquit();
void cleanup();
int listdevices();
void showaliases();
