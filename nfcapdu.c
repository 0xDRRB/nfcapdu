#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <err.h>
#include <errno.h>
#include <time.h>
#include <math.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <nfc/nfc.h>
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

#include <glib.h>

#include "color.h"
#include "nfcapdu.h"
#include "statusres.h"
#include "hist.h"
#include "log.h"
#include "util.h"
#include "nfclua.h"

// Default conf values
int conf_histsize = HISTSIZE;
size_t conf_rapdumaxsz = RAPDUMAXSZ;
size_t conf_capdumaxsz = CAPDUMAXSZ;
int conf_color = COLOR;
int conf_log = LOG;
char *logfile;
char *optconnstring;

nfc_modulation_type conf_modtype = MODTYPE;
nfc_baud_rate conf_nbr = NBR;

nfc_device *pnd;
nfc_context *context;
int haveconfig;
GKeyFile *ini;

char **aliaskeys;
gsize nbraliases;
char **words;
char *commands[] = { "quit", "alias", "history", "help", "rawmode", "apdumode", NULL };

FILE *fplog;

int mode = APDUMODE;

uint8_t luataguid[10];
size_t luataguidlen;

lua_State *L;

char *
rl_commands_generator(const char *text, int state)
{
	static int command_index, len;
	char *command;

	if (!state) {
		command_index = 0;
		len = strlen(text);
	}

	// search in wordlist
	while ((command = words[command_index++])) {
		if (strncmp(command, text, len) == 0)
			return strdup(command);
	}

	return NULL;
}

char **
rl_commands_completion(const char *text, int start, int end)
{
	// our list of completions is final - no path completion
	rl_attempted_completion_over = 1;
	return rl_completion_matches(text, rl_commands_generator);
}

static void
sighandler(int sig)
{
	printf("Caught signal %d\n", sig);
	if (pnd != NULL) {
		nfc_abort_command(pnd);
		nfc_close(pnd);
	}
	nfc_exit(context);
	exit(EXIT_FAILURE);
}

// return 1 if we have config
int
apdu_initconfig()
{
	char *home;
	char *cfile;
	int confpathsz;
	GError *gerr = NULL;

	if ((home=getenv("HOME")) == NULL) {
		err(EXIT_FAILURE, "Unable to get $HOME");
	}

	confpathsz = strlen(home) + 1 + strlen(CONFFILE) + 1;
	if ((cfile = (char *) malloc(confpathsz)) == NULL) {
		err(EXIT_FAILURE, "Memory allocation error: ");
	}

	if (snprintf(cfile, confpathsz, "%s/%s", home, CONFFILE) != confpathsz-1) {
		warnx("Configuration file path error");
	}

	ini = g_key_file_new();
	if (g_key_file_load_from_file(ini, cfile, G_KEY_FILE_KEEP_COMMENTS, &gerr) == FALSE) {
		if (gerr->code != G_FILE_ERROR_NOENT)
			warnx("Error loading config file: %s (%d)", gerr->message, gerr->code);
		g_key_file_free(ini);
		ini = NULL;
		if (gerr != NULL) g_clear_error(&gerr);
		free(cfile);
		return(0);
	}

	free(cfile);

	return(1);
}

// Transmit ADPU from hex string
int
strcardtransmit(nfc_device *pnd, const char *line, uint8_t *rapdu, size_t *rapdulen, int quiet)
{
	uint8_t *capdu = NULL;
	size_t capdulen = 0;

	// linelen >0 & even
	if (!strlen(line) || strlen(line) > conf_capdumaxsz*2)
		return(-1);

	if (!(capdu = malloc(strlen(line)/2))) {
		warn("malloc list error: ");
		nfc_close(pnd);
		nfc_exit(context);
		exit(EXIT_FAILURE);
	}

	if (!(capdulen = hex2array(line, capdu, strlen(line)/2))) {
		warnx("Invalid hex string!");
		if (capdu) free(capdu);
		return(-1);
	}

	if (cardtransmit(pnd, capdu, capdulen, rapdu, rapdulen, quiet) < 0) {
		if (capdu) free(capdu);
		return(-1);
	}

	if (capdu) free(capdu);
	return(0);
}

// Transmit ADPU from uint8_t array
int
cardtransmit(nfc_device *pnd, uint8_t *capdu, size_t capdulen, uint8_t *rapdu, size_t *rapdulen, int quiet)
{
	int res;
	size_t szPos;
	*rapdulen = conf_rapdumaxsz;
	char strstatus[128];

	uint16_t status;

	if (!quiet) {
		printf("%s=> ", conf_color ? YELLOW : "" );
		for (szPos = 0; szPos < capdulen; szPos++) {
			printf("%02x ", capdu[szPos]);
		}
		printf("%s\n", conf_color ? RESET : "");
	}

	if ((res = nfc_initiator_transceive_bytes(pnd, capdu, capdulen, rapdu, *rapdulen, -1)) < 0) {
		warnx("nfc_initiator_transceive_bytes error! %s", nfc_strerror(pnd));
		*rapdulen = 0;
		return(-1);
	}

	// log transmit
	apdu_logapdu(capdu, capdulen, 1);

	if (mode == APDUMODE) {
		status = (rapdu[res - 2] << 8) | rapdu[res - 1];

		if (status == S_SUCCESS || status == S_OK || status == S_MORE) {
			if (!quiet)
				printf("%s<= ", conf_color ? GREEN : "");
		} else {
			if (!quiet)
				printf("%s<= ", conf_color ? RED : "");
		}
	} else {
		if (!quiet)
			printf("%s<= ", conf_color ? GREEN : "");
	}

	for (szPos = 0; szPos < res; szPos++) {
		if (mode == APDUMODE) {
			status = (rapdu[res - 2] << 8) | rapdu[res - 1];
			if (szPos >= res - 2) {
				if (status == S_SUCCESS || status == S_OK || status == S_MORE) {
					if (!quiet)
						printf(BOLDGREEN);
				} else {
					if (!quiet)
						printf(BOLDRED);
				}
			}
		}
		if (!quiet)
			printf("%02x ", rapdu[szPos]);
	}

	if (!quiet) {
		if (mode == APDUMODE)
			if (status == S_MORE)
				printf("  ()");
	}

	if (!quiet)
		printf("%s(%d)\n", conf_color ? RESET : "", res);

	// log receive
	apdu_logapdu(rapdu, res, 0);

	if (mode == APDUMODE && !quiet) {
		if (status != S_SUCCESS && status != S_OK && status != S_MORE) {
			getstrstatus(status, strstatus, 128);
			if ((status & 0xff00) != 0x6100 && (status & 0xff00) != 0x9f00) {
				printf("Error: %s (0x%04x)\n", strstatus, status);
				return(-1);
			} else {
				printf("Warning: %s (0x%04x)\n", strstatus, status);
				return(0);
			}
		}
	}

	*rapdulen = (size_t)res;

	return(0);
}

int
listdevices()
{
	size_t device_count;
	nfc_connstring devices[8];

	// Scan readers/devices
	device_count = nfc_list_devices(context, devices, sizeof(devices)/sizeof(*devices));
	if (device_count <= 0) {
		warnx("Error: No NFC device found");
		return(0);
	}

	printf("Available readers/devices:\n");
	for (size_t d = 0; d < device_count; d++) {
		printf("  %lu: ", d);
		if (!(pnd = nfc_open(context, devices[d]))) {
			printf("nfc_open() failed\n");
		} else {
			printf("%s (connstring=\"%s\")\n", nfc_device_get_name(pnd), nfc_device_get_connstring(pnd));
			nfc_close(pnd);
		}
	}

	return(device_count);
}

static void
print_hex(const uint8_t *pbtData, const size_t szBytes)
{
	size_t  szPos;

	for (szPos = 0; szPos < szBytes; szPos++) {
		printf("%02X", pbtData[szPos]);
	}
}

void
failquit()
{
	if (words) free(words);
	if (aliaskeys) g_strfreev(aliaskeys);
	if (ini) g_key_file_free(ini);
	if (optconnstring) free (optconnstring);
	if (pnd) nfc_close(pnd);
	if (context) nfc_exit(context);
	exit(EXIT_FAILURE);
}

void cleanup()
{
	if (conf_log && logfile && fplog) {
		apdu_logstop();
		fclose(fplog);
	}
	if (words) free(words);
	if (aliaskeys) g_strfreev(aliaskeys);
	if (ini) g_key_file_free(ini);
	if (optconnstring) free (optconnstring);
	if (pnd) nfc_close(pnd);
	if (context) nfc_exit(context);
}

void
showaliases()
{
	int i = 0;
	char *val;
	GError *gerr = NULL;

	if (!nbraliases || !haveconfig) {
		printf("No alias\n");
		return;
	}

	printf("Defined aliases:\n");
	while (aliaskeys[i]) {
		val = g_key_file_get_value(ini, "aliases", aliaskeys[i], &gerr);
		if (gerr) {
			g_clear_error(&gerr);
		} else {
			printf("  %s = %s%s\n", aliaskeys[i], val, strlen(val) == 0 ? "<empty!>" : "");
			g_free(val);
		}
		i++;
	}
}

void
switchmode(nfc_device *pnd, nfc_modulation *mod, nfc_target *nt, int targetmode)
{
	int res;

	if ((res = nfc_initiator_deselect_target(pnd)) < 0) {
		warnx("Unable to deselect target! (%d)", res);
	} else {
		printf("%d target(s) deselected\n", res);
	}

	if (nfc_initiator_init(pnd) != NFC_SUCCESS) {
		nfc_perror(pnd, "nfc_initiator_init");
		failquit();
	}

	/*
	// Drop the field for a while
	nfc_device_set_property_bool(pnd, NP_ACTIVATE_FIELD, false);
	usleep(200*1000);
	*/
	if (targetmode == RAWMODE)
		nfc_device_set_property_bool(pnd, NP_EASY_FRAMING, false);
	else
		nfc_device_set_property_bool(pnd, NP_EASY_FRAMING, true);

	nfc_device_set_property_bool(pnd, NP_HANDLE_PARITY, true);
	nfc_device_set_property_bool(pnd, NP_HANDLE_CRC, true);
	nfc_device_set_property_bool(pnd, NP_INFINITE_SELECT, false);

	if (nfc_initiator_select_passive_target(pnd, *mod, NULL, 0, nt) > 0) {
		printf("%s (%s) tag found. UID: %s",
				str_nfc_modulation_type(mod->nmt), str_nfc_baud_rate(mod->nbr), conf_color ? CYAN : "");
		print_hex(nt->nti.nai.abtUid, nt->nti.nai.szUidLen);
		printf("%s\n", conf_color ? RESET : "");
	} else {
		warnx("Error: No %s (%s) tag found!", str_nfc_modulation_type(mod->nmt), str_nfc_baud_rate(mod->nbr));
		failquit();
	}

	mode = targetmode;
	apdu_logmode(mode);
}

int
main(int argc, char**argv)
{
	int i, j;
	char *in;
	char *fhistory;

	nfc_target nt;

	nfc_modulation mod = {
		.nmt = NMT_ISO14443A,
		.nbr = NBR_106
	};

	uint8_t *resp;
	size_t respsz;

	int retopt;
	int optlistdev = 0;
	char *optluascript = NULL;

	GError *gerr = NULL;
	char *aliasval;
	int nbr_commands = 0;

	// FIXME not pretty
	int wordsneedfree = 0;

	char *conf_mod;
	char *conf_nbr;

	char *tmplogfile = NULL;

	while ((retopt = getopt(argc, argv, "hlrd:s:")) != -1) {
		switch (retopt) {
		case 'l':
			optlistdev = 1;
			break;
		case 'r':
			mode = RAWMODE;
			break;
		case 'd':
			optconnstring = strdup(optarg);
			break;
		case 's':
			optluascript = strdup(optarg);
			break;
		case 'h':
			printhelp(argv[0]);
			return(EXIT_FAILURE);
		default:
			printhelp(argv[0]);
			return(EXIT_FAILURE);
		}
	}

	if (signal(SIGINT, &sighandler) == SIG_ERR) {
		printf("Error: Can't catch SIGINT\n");
		return(EXIT_FAILURE);
	}

	if (signal(SIGTERM, &sighandler) == SIG_ERR) {
		printf("Error: Can't catch SIGTERM\n");
		return(EXIT_FAILURE);
	}

	// Initialize libnfc and set the nfc_context
	nfc_init(&context);
	if (context == NULL) {
		printf("Error: Unable to init libnfc (malloc)\n");
		exit(EXIT_FAILURE);
	}

	if (optlistdev) {
		listdevices();
		nfc_exit(context);
		return(EXIT_SUCCESS);
	}

	printf(NFCAPDUVERSION);

	// load configuration
	haveconfig = apdu_initconfig();

	// get command history from config file (or use default)
	if (haveconfig) {
		conf_histsize = g_key_file_get_integer(ini, "general", "histsize", &gerr);
		if (gerr) {
			if (gerr->code == G_KEY_FILE_ERROR_INVALID_VALUE)
				warnx("Invalid value for 'histsize' in configuration file. Using default.");
			conf_histsize = HISTSIZE;
			g_clear_error(&gerr);
		}
	}

	// get color enable from config file (or use default)
	if (haveconfig) {
		conf_color = g_key_file_get_boolean(ini, "general", "color", &gerr);
		if (gerr) {
			if (gerr->code == G_KEY_FILE_ERROR_INVALID_VALUE)
				warnx("Invalid boolean for 'color' in configuration file. Using default.");
			conf_color = COLOR;
			g_clear_error(&gerr);
		}
	}

	// get log enable from config file
	if (haveconfig) {
		conf_log = g_key_file_get_boolean(ini, "general", "enable_log", &gerr);
		if (gerr) {
			if (gerr->code == G_KEY_FILE_ERROR_INVALID_VALUE)
				warnx("Invalid boolean for 'enable_log' in configuration file. Using default.");
			// use default
			conf_log = LOG;
			g_clear_error(&gerr);
		}
		if (conf_log) {
			tmplogfile = g_key_file_get_string(ini, "general", "logfile", &gerr);

			if (gerr) {
				if (gerr->code == G_KEY_FILE_ERROR_INVALID_VALUE)
					warnx("Invalid string for 'logfile' in configuration file. Using default.");
				// use default
				logfile = strdup(LOGFILE);
				g_clear_error(&gerr);
			} else {
				if (!strlen(tmplogfile)) {
					warnx("Error: Empty logfile name!");
					logfile = strdup(LOGFILE);
				} else {
					logfile = strdup(tmplogfile);
				}
				g_free(tmplogfile);
			}
		}
	}

	// get max size of response APDU buffer from config file (or use default)
	if (haveconfig) {
		conf_rapdumaxsz = g_key_file_get_integer(ini, "general", "rapdumaxsz", &gerr);
		if (gerr) {
			if (gerr->code == G_KEY_FILE_ERROR_INVALID_VALUE)
				warnx("Invalid value for 'rapdumaxsz' in configuration file. Using default.");
			conf_rapdumaxsz = RAPDUMAXSZ;
			g_clear_error(&gerr);
		}
	}

	// get max size of command APDU buffer from config file (or use default)
	if (haveconfig) {
		conf_capdumaxsz = g_key_file_get_integer(ini, "general", "capdumaxsz", &gerr);
		if (gerr) {
			if (gerr->code == G_KEY_FILE_ERROR_INVALID_VALUE)
				warnx("Invalid value for 'capdumaxsz' in configuration file. Using default.");
			conf_capdumaxsz = CAPDUMAXSZ;
			g_clear_error(&gerr);
		}
	}

	// get modulation type from config file
	if (haveconfig) {
		conf_mod = g_key_file_get_string(ini, "general", "modtype", &gerr);
		if (gerr) {
			g_clear_error(&gerr);
		} else {
			g_strstrip(conf_mod);
			if (strcmp(conf_mod, "NMT_ISO14443A") == 0)
				mod.nmt = NMT_ISO14443A;
			else if (strcmp(conf_mod, "NMT_JEWEL") == 0)
				mod.nmt = NMT_JEWEL;
			else if (strcmp(conf_mod, "NMT_ISO14443B") == 0)
				mod.nmt = NMT_ISO14443B;
			else if (strcmp(conf_mod, "NMT_ISO14443BI") == 0)
				mod.nmt = NMT_ISO14443BI;
			else if (strcmp(conf_mod, "NMT_ISO14443B2SR") == 0)
				mod.nmt = NMT_ISO14443B2SR;
			else if (strcmp(conf_mod, "NMT_ISO14443B2CT") == 0)
				mod.nmt = NMT_ISO14443B2CT;
			else if (strcmp(conf_mod, "NMT_FELICA") == 0)
				mod.nmt = NMT_FELICA;
			else if (strcmp(conf_mod, "NMT_DEP") == 0)
				mod.nmt = NMT_DEP;
#ifdef LIBNFC18
			else if (strcmp(conf_mod, "NMT_BARCODE") == 0)
				mod.nmt = NMT_BARCODE;
			else if (strcmp(conf_mod, "NMT_ISO14443BICLASS") == 0)
				mod.nmt = NMT_ISO14443BICLASS;
#endif
			else
				warnx("Invalid 'modtype' in configuration file. Using default (NMT_ISO14443A).");
			g_free(conf_mod);
		}
	}

	// get baud rate from config file
	if (haveconfig) {
		conf_nbr = g_key_file_get_string(ini, "general", "baudrate", &gerr);
		if (gerr) {
			g_clear_error(&gerr);
		} else {
			g_strstrip(conf_nbr);
			if (strcmp(conf_nbr, "NBR_106") == 0)
				mod.nbr = NBR_106;
			else if (strcmp(conf_nbr, "NBR_212") == 0)
				mod.nbr = NBR_212;
			else if (strcmp(conf_nbr, "NBR_424") == 0)
				mod.nbr = NBR_424;
			else if (strcmp(conf_nbr, "NBR_847") == 0)
				mod.nbr = NBR_847;
			else
				warnx("Invalid 'baudrate' in configuration file. Using default (NBR_106).");
			g_free(conf_nbr);
		}
	}

	if (optconnstring) {
		// Open, using specified NFC device
		pnd = nfc_open(context, optconnstring);
	} else {
		// Open, using the first available NFC device which can be in order of selection:
		//   - default device specified using environment variable or
		//   - first specified device in libnfc.conf (/etc/nfc) or
		//   - first specified device in device-configuration directory (/etc/nfc/devices.d) or
		//   - first auto-detected (if feature is not disabled in libnfc.conf) device
		pnd = nfc_open(context, NULL);
	}

	if (pnd == NULL) {
		errx(EXIT_FAILURE, "Error: Unable to open NFC device!");
	}

	// Set opened NFC device to initiator mode
	if (nfc_initiator_init(pnd) != NFC_SUCCESS) {
		nfc_perror(pnd, "nfc_initiator_init");
		exit(EXIT_FAILURE);
	}

	printf("NFC reader: %s (%s) opened\n", nfc_device_get_name(pnd), nfc_device_get_connstring(pnd));

	if (mode == RAWMODE) {
		nfc_device_set_property_bool(pnd, NP_EASY_FRAMING, false);
		nfc_device_set_property_bool(pnd, NP_HANDLE_PARITY, true);
		nfc_device_set_property_bool(pnd, NP_HANDLE_CRC, true);
		nfc_device_set_property_bool(pnd, NP_INFINITE_SELECT, false);
	}
	if (nfc_initiator_select_passive_target(pnd, mod, NULL, 0, &nt) > 0) {
		printf("%s (%s) tag found. UID: %s",
				str_nfc_modulation_type(mod.nmt), str_nfc_baud_rate(mod.nbr), conf_color ? CYAN : "");
		print_hex(nt.nti.nai.abtUid, nt.nti.nai.szUidLen);
		luataguidlen = nt.nti.nai.szUidLen;
		for (i = 0; i < luataguidlen; i++) {
			luataguid[i] = nt.nti.nai.abtUid[i];
		}
		printf("%s\n", conf_color ? RESET : "");
	} else {
		warnx("Error: No %s (%s) tag found!", str_nfc_modulation_type(mod.nmt), str_nfc_baud_rate(mod.nbr));
		failquit();
	}

	if (conf_log && logfile) {
		fplog = fopen(logfile, "a");
		if (fplog == NULL) {
			warn("Error opening logfile: ");
			warnx("Logging disabled!");
			conf_log = 0;
		} else {
			printf("Logging enabled (%s openned)\n", logfile);
			apdu_logstart();
		}
	}

	// Load and run Lua script
	if(optluascript) {
		L = luaL_newstate();

		if (!L) {
			warnx("Lua state creation failed!");
			failquit();
		}

		luaL_openlibs(L);
		lua_register(L, "sendstrapdu", lua_sendstrapdu);
		lua_register(L, "sendapdu", lua_sendapdu);
		lua_register(L, "getuid", lua_getuid);

		printf("\n");
		if (luaL_dofile(L, optluascript) != LUA_OK) {
			puts(lua_tostring(L, lua_gettop(L)));
			lua_pop(L, lua_gettop(L));
			lua_close(L);
			if(optluascript)
				free(optluascript);
			failquit();
		}

		lua_close(L);
		free(optluascript);

		cleanup();
		return(EXIT_SUCCESS);
	}

	// show aliases & load readline completion
	if (haveconfig) {
		aliaskeys = g_key_file_get_keys(ini, "aliases", &nbraliases, &gerr);
		if (gerr) {
			warnx("%s", gerr->message);
			words = commands;
			g_clear_error(&gerr);
		} else {
			printf("%lu alias%s loaded\n", nbraliases, nbraliases>1 ? "" : "");
			// merge commands to aliases to completion wordslist
			while (commands[nbr_commands]) nbr_commands++;
			if ((words = malloc(sizeof(char *) * (nbr_commands+nbraliases + 1))) == NULL) {
				warn("Words malloc Error: ");
				failquit();
			}
			i = 0;
			while (commands[i]) {
				words[i] = commands[i];
				i++;
			}
			j = 0;
			while (aliaskeys[j]) {
				words[i] = aliaskeys[j];
				i++; j++;
			}
			words[i] = 0;
			wordsneedfree = 1; // enable free(words) when we quit
		}
	} else {
		words = commands;
	}

	printf("Use 'help' to get a list of available commands.\n");

	// Load commands history
	apdu_inithistory(&fhistory);

	// Enable completion
	rl_attempted_completion_function = rl_commands_completion;

	// allocate R-APDU buffer
	if ((resp = malloc(sizeof(uint8_t) * conf_rapdumaxsz)) == NULL) {
		warn("resp[] malloc Error: ");
		failquit();
	}
	respsz = conf_rapdumaxsz;

	while ((in = readline(mode == APDUMODE ? "APDU> " : "RAW> ")) != NULL) {
		if (strlen(in) && !isblankline(in)) {
			// strip whitespace
			g_strstrip(in);
			// add to commands history
			apdu_addhistory(in);
			if (strcmp(in, "alias") == 0) {
				showaliases();
				free(in);
				continue;
			}
			if (strcmp(in, "history") == 0) {
				apdu_disphist();
				free(in);
				continue;
			}
			if (strcmp(in, "help") == 0) {
				apdu_help();
				free(in);
				continue;
			}
			if (strcmp(in, "rawmode") == 0) {
				switchmode(pnd, &mod, &nt, RAWMODE);
				free(in);
				continue;
			}
			if (strcmp(in, "apdumode") == 0) {
				switchmode(pnd, &mod, &nt, APDUMODE);
				free(in);
				continue;
			}
			if (strcmp(in, "quit") == 0) {
				free(in);
				break;
			}
			if (!nbraliases) {
				// no alias in config file
				if (strcardtransmit(pnd, in, resp, &respsz, 0) < 0) {
					warnx("cardtransmit error!");
				}
			} else {
				// look for alias
				aliasval = g_key_file_get_value(ini, "aliases", in, &gerr);
				if (gerr) {
					// alias not found, send what i have
					if (strcardtransmit(pnd, in, resp, &respsz, 0) < 0) {
						warnx("cardtransmit error!");
					}
					g_clear_error(&gerr);
				} else if (!strlen(aliasval)) {
					// alias resolv to ""
					printf("This alias resolv to an empty string. Fix your "CONFFILE" please.\n");
				} else {
					// alias found, use that
					if (strcardtransmit(pnd, aliasval, resp, &respsz, 0) < 0) {
						warnx("cardtransmit error!");
					}
				}
				g_free(aliasval);
			}
		}
		if (in)
			free(in);
	}
	printf("\n");

	apdu_closehistory(fhistory);

	if (conf_log && logfile && fplog) {
		apdu_logstop();
		fclose(fplog);
	}

	if (haveconfig) {
		if (aliaskeys)
			g_strfreev(aliaskeys);
		if (ini)
			g_key_file_free(ini);
	}

	if (optconnstring)
		free(optconnstring);

	if (optluascript)
		free(optluascript);

	if (words && wordsneedfree)
		free(words);

	if (resp)
		free(resp);

	// Close NFC device
	nfc_close(pnd);
	// Release the context
	nfc_exit(context);

	printf("Bye. Have a nice day\n");

	return(EXIT_SUCCESS);
}
